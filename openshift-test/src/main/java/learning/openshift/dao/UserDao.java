package learning.openshift.dao;

import java.util.List;

import learning.openshift.entity.User;

public interface UserDao {

	List<User> getUsers();
	
}
