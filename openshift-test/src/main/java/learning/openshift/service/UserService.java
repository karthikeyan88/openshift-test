package learning.openshift.service;

import java.util.List;

import learning.openshift.entity.User;

public interface UserService {

	List<User> getUsers();
	
}
